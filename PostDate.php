<?php

namespace Stitchdx\WordPress\Traits;

/**
 * Class PostDate
 *
 * @package Stitchdx\WordPress\Traits
 *
 * @property \WP_Post $post
 */
trait PostDate {

	/**
	 * Get the post date.
	 *
	 * @param string $format The PHP date format.
	 *
	 * @return string
	 */
	public function postDate( $format = '' ) {
		$format = empty( $format ) ? get_option( 'date_format' ) : $format;

		return (string) get_the_date( $format, $this->post );
	}

	/**
	 * Get the post date object.
	 *
	 * @return \DateTime
	 */
	public function postDateObject() {
		$timezone = get_option( 'timezone_string' );
		if ( empty( $timezone ) ) {
			$timezone = str_replace( [ 'UTC-', 'UTC+' ], 'UTC ', get_option( 'gmt_offset', 'UTC' ) );
		}
		$date = new \DateTime( $this->post->post_date_gmt );
		$date->setTimezone( new \DateTimeZone( $timezone ) );

		return $date;
	}

}